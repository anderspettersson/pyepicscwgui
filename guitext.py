#!/usr/bin/python
# -*- coding: utf-8 -*-
#Comment bara för att testa git on Atom!
import random
import wx
from datetime import datetime
from epics import caget, caput, PV

class Example(wx.Frame):

    def __init__(self, *args, **kw):
        super(Example, self).__init__(*args, **kw)

        self.InitUI()


    def InitUI(self):
        pnl = wx.Panel(self)
        #Define a font?
        fontBold = wx.Font(38, wx.SCRIPT, wx.NORMAL, wx.BOLD)
        fontNormal = wx.Font(28, wx.SCRIPT, wx.NORMAL, wx.NORMAL)

        #Unit lables
        st1 = wx.StaticText(pnl, label='Flow (l/min):', pos=(10,70))
        st2 = wx.StaticText(pnl, label='Temp (°C):', pos=(10,140))
        st1.SetFont(fontNormal)
        st2.SetFont(fontNormal)

        #Input/Output lables
        st3 = wx.StaticText(pnl, label='Inlet', pos=(260,20))
        st4 = wx.StaticText(pnl, label='Outlet', pos=(450,20))
        st3.SetFont(fontNormal)
        st4.SetFont(fontNormal)

        #The button fo rupdate and binding to function when pressed
        self.openVbtn = wx.Button(pnl, label='Open Valve', pos=(250, 270))
        self.openVbtn.Bind(wx.EVT_BUTTON, self.OpenValve)
        self.closeVbtn = wx.Button(pnl, label='Close Valve', pos=(340, 270))
        self.closeVbtn.Bind(wx.EVT_BUTTON, self.CloseValve)
        #The button fo rupdate and binding to function when pressed
        cbtn = wx.Button(pnl, label='Exit', pos=(450, 270))
        cbtn.Bind(wx.EVT_BUTTON, self.OnExit)

        #The lables that get updated!
        self.stInFlow = wx.StaticText(pnl, label='', pos=(260, 60))
        self.stInTemp = wx.StaticText(pnl, label='', pos=(260, 130))
        self.stOutFlow = wx.StaticText(pnl, label='', pos=(450, 60))
        self.stOutTemp = wx.StaticText(pnl, label='', pos=(450, 130))
        self.stStatus = wx.StaticText(pnl, label='Valve OPEN', pos=(200, 200))
        self.stInFlow.SetFont(fontBold)
        self.stInTemp.SetFont(fontBold)
        self.stOutFlow.SetFont(fontBold)
        self.stOutTemp.SetFont(fontBold)
        self.stStatus.SetFont(fontBold)


        #Texts fro showing teh opening/closing variables
        stOpeningLab = wx.StaticText(pnl, label='Opening:', pos=(110, 300))
        self.stClosingLab = wx.StaticText(pnl, label='Closing:', pos=(110, 320))
        self.stOpening = wx.StaticText(pnl, label='', pos=(170, 300))
        self.stClosing = wx.StaticText(pnl, label='', pos=(170, 320))

        #Texts för diagnos
        self.stDiag = wx.StaticText(pnl, label='Nothing yet', pos=(600, 4))

        #Texts for showing the open/closed variables
        self.stOpenLab = wx.StaticText(pnl, label='Open:', pos=(10, 300))
        self.stClosedLab = wx.StaticText(pnl, label='Closed:', pos=(10, 320))
        self.stOpen = wx.StaticText(pnl, label='', pos=(70, 300))
        self.stClosed = wx.StaticText(pnl, label='', pos=(70, 320))

        self.SetSize((700, 400))
        self.SetTitle('...::: Cooling Water :::...')
        self.Centre()
        self.Show(True)



        #Add variables########################
        # Local
        self.opening = False
        self.closing = False

        #From PVs
        self.pvInFlow = PV('LabS-Utgard:WTRC-FIT-01:Value')
        self.pvInTemp = PV('LabS-Utgard:WTRC-TIT-01:Value')
        self.pvOutFlow = PV('LabS-Utgard:WTRC-FIT-02:Value')
        self.pvOutTemp = PV('LabS-Utgard:WTRC-TIT-02:Value')
        #Använd nedan som readback på ventilen!
        self.pvValveOpen = PV('LabS-Utgard:WTRC-YSV-01:Opened')
        self.pvValveClosed = PV('LabS-Utgard:WTRC-YSV-01:Closed')


        self.pvOpenValve = PV('LabS-Utgard:WTRC-YSV-01:ManOpenCmd')
        self.pvCloseValve = PV('LabS-Utgard:WTRC-YSV-01:ManCloseCmd')

        #Start calling the function with a timer. Thsi will Start
        # A continuos update loop?
        self.on_timer()

    def on_timer(self):
        cnt = 0
#        if (True == self.opening) and (True == pvValveOpen.value): #This logic is wrong!!!
        if  (True == self.pvValveOpen.value):
            #self.opening = False
            self.stStatus.SetForegroundColour('red')
            self.stStatus.SetLabel('Valve CLOSED')
            #print('OPEN')

#        elif (True == self.closing) and (True == pvValveClosed.value):#This logic is wrong!!!
        elif (True == self.pvValveClosed.value):
            #self.closing = False
            self.stStatus.SetLabel('Valve OPEN')
            self.stStatus.SetForegroundColour('green')
            #print('CLOSED')

        self.stOpening.SetLabel(str(self.opening))
        self.stClosing.SetLabel(str(self.closing))
        try:
            self.stOpen.SetLabel(str(self.pvValveOpen.value))
            self.stDiag.SetLabel("Data Online")
            self.stDiag.SetForegroundColour('black')
        except:

            print "Valve Open not read" + str(datetime.now())
            self.stDiag.SetLabel("No Data!")
            self.stDiag.SetForegroundColour('red')
            cnt += 1
        try:
            self.stClosed.SetLabel(str(self.pvValveClosed.value))
            self.stDiag.SetLabel("Data Online")
            self.stDiag.SetForegroundColour('black')
        except:

            print "Valve closed not read" + str(datetime.now())
            self.stDiag.SetLabel("No Data!")
            self.stDiag.SetForegroundColour('red')
            cnt += 1
        try:
            self.stInFlow.SetLabel(str(round(self.pvInFlow.value, 1)))
            self.stDiag.SetLabel("Data Online")
            self.stDiag.SetForegroundColour('black')
        except:

            print "InFlow not read" + str(datetime.now())
            self.stDiag.SetLabel("No Data!")
            self.stDiag.SetForegroundColour('red')
            cnt += 1

        try:
            self.stInTemp.SetLabel(str(round(self.pvInTemp.value,1)))
            self.stDiag.SetLabel("Data Online")
            self.stDiag.SetForegroundColour('black')
        except:

            print "InTemp not read" + str(datetime.now())
            self.stDiag.SetLabel("No Data!")
            self.stDiag.SetForegroundColour('red')
            cnt += 1

        try:
            self.stOutFlow.SetLabel(str(round(self.pvOutFlow.value, 1)))
            self.stDiag.SetLabel("Data Online")
            self.stDiag.SetForegroundColour('black')
        except:

            print "OutFlow not read" + str(datetime.now())
            self.stDiag.SetLabel("No Data!")
            self.stDiag.SetForegroundColour('red')
            cnt += 1

        try:
            self.stOutTemp.SetLabel(str(round(self.pvOutTemp.value,1)))
            self.stDiag.SetLabel("Data Online")
            self.stDiag.SetForegroundColour('black')
        except:

            print "Out temp not read" + str(datetime.now())
            self.stDiag.SetLabel("No Data!")
            self.stDiag.SetForegroundColour('red')
            cnt += 1

        print "Errors  " + str(cnt) + "  "  + str(datetime.now())
        wx.CallLater(1000, self.on_timer)


    def OnExit(self, e):
        self.Close(True)

    def OpenValve(self, e):
        #self.openVbtn.SetLabel('hej')
        self.stStatus.SetForegroundColour('black')
        self.pvOpenValve.value = 1
        self.stStatus.SetLabel('OPENING Valve')
        self.opening = True
        self.closing = False

    def CloseValve(self, e):
        #self.closeVbtn.SetLabel('stäng')
        self.stStatus.SetForegroundColour('black')
        self.pvCloseValve.value = 1
        self.stStatus.SetLabel('CLOSING Valve')
        self.opening = False
        self.closing = True


def main():

    ex = wx.App()
    Example(None)
    ex.MainLoop()


if __name__ == '__main__':
    main()
